## Prueba Técnica en React - Buscador de Música con Last.fm API
### Objetivo
Crear una aplicación web utilizando React que permita a los usuarios buscar música utilizando la API de Last.fm. La aplicación deberá mostrar los últimos 5 álbumes, canciones y artistas relacionados con la búsqueda realizada.

### Pasos a seguir

1. #### Configuración del Proyecto
   * Crea un nuevo proyecto de React utilizando tu método preferido.
   * Asegúrate de tener Node.js y npm instalados en tu sistema.
   
2. #### Integración con la API de Last.fm
   * Utiliza la siguiente apikey para la integración con Last.fm: 
   API KEY = fcd492e73eaf5db3fc46164916b00df9
   SHARED SECRET = 755dbc6c47b720bee54298227fb95cb6

3. #### Componentes Básicos
   * Crea un componente SearchBar que contendrá el campo de búsqueda.
   * Crea un componente SearchResults para mostrar los resultados de la búsqueda (álbumes, canciones y artistas).
   * Crea un componente ResultItem para mostrar un elemento individual (álbum, canción o artista) en la lista de resultados.
   
4. #### Implementación de la Búsqueda
   * Mostrar resultados de artistas, canciones y álbumes en función de la consulta de búsqueda.
   * Utiliza una técnica de para evitar llamadas excesivas a la API mientras el usuario escribe en el campo de búsqueda.

5. #### Llamadas a la API y Mostrar Resultados
   * Cuando el usuario escriba en el campo de búsqueda, realiza una llamada a la API de Last.fm utilizando la URL adecuada, incluyendo la búsqueda y tu API Key.
   * Procesa los resultados de la API y actualiza el estado de tu componente SearchResults.
   
6. #### Mostrar los Últimos 5 Elementos
   * Filtra los resultados para mostrar solo los últimos 5 álbumes, canciones y artistas.

7. #### Diseño y Estilos
   * Agrega estilos CSS para mejorar la apariencia de la aplicación.
   * Crea una interfaz de usuario intuitiva y receptiva.

8. #### Pruebas y Validación
   * Prueba exhaustivamente la aplicación para asegurarte de que funciona correctamente.
   * Asegúrate de que la búsqueda, la API y la actualización de resultados se comporten según lo esperado.

9. #### Extras (Opcionales)
   * Mejora el diseño y la experiencia de usuario según tus habilidades y creatividad.
   * Implementa paginación para mostrar más resultados.
   * Agrega opciones de filtrado para los resultados.
   * Asegúrate de que los componentes se vean bien en diferentes dispositivos y tamaños de pantalla.

### Entrega
   * Sube tu código a un repositorio en GitHub u otra plataforma de control de versiones.
   * Proporciona instrucciones claras en el archivo README.md sobre cómo instalar, ejecutar y utilizar tu aplicación.

Este proyecto es una oportunidad para demostrar sus habilidades en el desarrollo de aplicaciones React JS y la integración con APIs externas. ¡Diviértase codificando!

¡Buena suerte!
